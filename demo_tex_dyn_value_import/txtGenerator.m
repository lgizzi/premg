function txtGenerator(varArray,labelArray,refObject)
%% textGenerator
% This function is dedicated to the generation of a txt twin to a figure
% regarding variables and samples. It saves labeled variables or samples
% into a txt file at a place of the users choice, which tehn can be read 
% e.g. by latex (see therefore the tex script in the repository, called 
% 'demo_tex_dyn_value_import' -> doc.tex and defs.txt).
%
% INPUT:
% varArray: num column array [n ;...; n+1] with variables, which have to be saved in the txt
% labelArray: corresponding label array to the varArray as char column array 
% relObject: string or char of the referenced figure
%
% OUTPUT:
% txt file in the current directory with the variables to a certain figure 
%
%
% Author: Dominic Hillerkuss
% Date: 15th October 2019
% ------------------------------------------------------------------------
%% Only for testing
% refObject = 'SuperPlot'; % INPUT
% labelArray = ['var1';'var2'];  % INPUT
% varArray = [19;14];  % INPUT
% ------------------------------------------------------------------------
%%

% checking the input (just in case)
try
    if size(varArray,2) > size(varArray,1)
        disp('Transposing input ...')
        varArray = transpose(varArray);
        labelArray  = transpose(labelArray);
    end
    
    if isstring(labelArray)
        disp('String to char conversion ofthe input ...')
        labelArray = cell2mat(convertStringsToChars(labelArray));
    end
catch
    disp('ERROR: Wrong format of the input variables!')
    keyboard
end

% Directory management
pwdTmp = pwd;
fprintf('\n')
disp('txtGenerator'); 
selpath = uigetdir(pwd,'Select a directory you want to save the txt file!');
fprintf('Saving in directory: %s',selpath)
cd (selpath)

% File handler
if isfile(join([refObject,'.txt']))
    found = questdlg('There is already a txt file for the same figure. Do you want to delete that existing file and save the new one?', ...
        'Warning!', ...
        'Delete and Save','Cancel','Delete and Save');
    switch found
        case 'Delete and Save'
            delete (join([refObject,'.txt']))
            disp('Previously existed file has been deleted.')
        case 'Cancel'
            disp('Funciton aborted.')
            return
    end 
end
disp('Creating a new txt file...'); 

% this is going to be the filename
fid = fopen(fullfile(selpath, join([refObject,'.txt'])), 'a'); 
if fid == -1, error('ERROR: Can not open file!'); keyboard; end

% this is the header
fprintf(fid, '**** This Is The Header **** \n'); 
fprintf(fid, 'Related Figure: %s\n', refObject);
fprintf(fid, 'Status: %s\n', datestr(now, 0)); 
fprintf(fid, '**************************** \n'); 

% this is goingto be the conten
if length(varArray) == 1
    yourMsg = [labelArray,' = ',num2str(varArray)];t
    fprintf(fid, '%s\n', yourMsg);
else
    for k = 1:length(varArray)
        yourMsg = [labelArray(k,:),' = ',num2str(varArray(k))];
        fprintf(fid, '%s\n', yourMsg);
    end
end

% closing
fclose(fid);
fprintf('txt-file for < %s > has been generated!\n',refObject)
fprintf('\n')
cd (pwdTmp)
end

