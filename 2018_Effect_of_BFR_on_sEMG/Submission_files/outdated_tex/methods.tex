\subsection{Participants}
Healthy men and women aged 18-45 years were recruited based on a self-reported general state of good health, with no history of orthopaedic, cardiovascular or neurological conditions. Exclusion criteria were: recent (\ie less than 6 months) injuries in the upper limb, presence of neurological diseases, hypertension, capillary frailty, deep veins thrombosis, haemophilia, or any disease correlated with altered (i.e. either reduced or increased) blood coagulation or in presence of pregnancy or suspicion of. Subjects using antidepressant, blood thinning/thickening or pain drugs were also excluded. In total, \nsubjects healthy young individuals (\meanage, \meanheigh, \meanweight) were admitted to the study after signing an informed consent form; all of them, except for one, defined themselves as 'right-handed'. All the procedures complied with the Declaration of Helsinki and were authorized by the local Ethical Committee.% (approval number \ethicsapproval). 
	
\subsection{Data recording}
\textit{Recorded variables - }
Blood pressure (systolic and diastolic), elbow torque and high-density electromyographic activity from the biceps brachii were recorded.
Further, ratings of discomfort were quantified via a numerical scale rating (NSR, \cite{williamson2005pain}) ranging between 1 and 10. The subjects were instructed to associate 1 with 'no discomfort at all' and 10 with 'the worst imaginable discomfort'; data was reported verbally and recorded for each trial and condition immediately after the end of the contraction and prior to the deflation of the cuff. Data were then manually transferred to comma-separated files andelectronically stored for further analyses. 

\textit{Experimental apparatus and subject positioning - }
Subjects were seated with a straight back on a chair and were instructed not to touch the back rest. Their right arm was placed in a custom-made aluminium rig (ALF, IMSB, University of Stuttgart, Stuttgart, Germany) whose sensing element was a 500Nm flange-to-flange torque sensor (TR12, cct transducers, Turin, Italy). The height of the chair was adjusted so that a $90\degree$ angle for the elbow and for the abduction and flexion of the shoulder were obtained (see figure \ref{fig_exp_apparatus}). Finally, for the duration of the task, the wrist was restrained with inextensible belts (separated from the skin by a small foam cushion for comfort) to maintain it between pronation and supination. The length of the lever arm was noted down and checked for consistency at the beginning of each task. After the subject was positioned and restrained and prior to the beginning of each recording, a gravity compensation was performed by resetting the offset of the force amplifier. 

{\centering Figure \ref{fig_exp_apparatus} here \par}

\textit{Recording hardware and software - }
Torque data was amplified (FORZA, OTBioelettronica, Turin, Italy; gain:1000V/V). Torque and electromygraphic data were recorded by means of an EMG amplifier  (QUATTROCENTO, OTBioelettronica, Turin, Italy; gain: 150V/V, A/D depth: 16 bits per sample, sampling frequency: 2048 samples per second, band-pass filter: 10-900Hz, 8\textsuperscript{th}-order Bessel filter). Data were recorded via  a proprietary recording software (OTBiolab V.2.06, OTBioelettronica, Turin, Italy) and post-processed via custom written software in Matlab (2016b, the Mathworks, Natick, Massachusetts). Normalized torque values were displayed to the subject (visual feedback) on a computer screen (13 inches, 60Hz refresh rate) which was placed roughly 1.2 meters away from the subjects, in their line of sight.

\textit{Subject preparation - }
Subjects' skin was gently abraded (Everi Skin Preparation Gel, Spes Medica, Genova, Italy) and cleaned with alcohol (Kodan Tinktur forte, Sch\"ulke\,\&\,Mayr, Norderstedt, Germany). A 64-channels square-shaped EMG matrix (ELSCH064NM3, OTBioelettronica, Turin, Italy) with 10mm inter-electrode distance was placed such that the centre of the array would fall approximately at 1/3 of the distance between the acromion and the fossa cubit. The rows of channels were aligned with the estimated direction of the muscle fibres, following SENIAM recommendations \citep{Hermens2000}. The array was fixed with surgical tape (Fixomull Stretch, BSN Medical GmbH, Hamburg, Germany). Prior to placement, the holes of the array foam were filled with dense electro-conductive gel (AC CREAM250V-3, Spes Medica, Genova, Italy). Reference electrodes were placed on the medial epicondyle of the right elbow (local reference for the EMG preamplifier) and on the right acromion (reference for main amplifier). HDEMG data was recorded in referenced monopolar mode.\\

\subsection{Experimental Tasks}
\textit{Experimental protocol - }
Prior to the beginning of the measurements, the subjects were instructed about the scopes and procedures of the experiment. After a first blood pressure measurement the scientists prepared the subject for HDEMG and adjusted the apparatus. At this point a second blood pressure measurement was taken, the values retained, and the LP and HP levels computed. After a visual assessment of EMG signal quality, the subjects were given a few minutes to familiarize with the experimental apparatus and the visual feedback, then the measurements started. 

\textit{Blood pressure measurement - }
The blood pressure measurement was performed via an analog sphygmomanometer and cuff (boso classic, Bosch\,+\,Sohn, Jungingen, Germany) and a stethoscope (bososcope cardio, Bosch\,+\,Sohn, Jungingen, Germany) by an experienced operator. The measurement was always performed by the same researcher for one subject and was repeated twice: the first time at the arrival of the subject, the second prior to the beginning of the tasks. This was to familiarize with the the blood pressure measurement setup. The latest measurement was used to determine the levels of pressure for the experiment. 

\textit{Torque measurement - }
The maximal voluntary contraction (MVC) was recorded three times; repetitions were separated by at least three minutes, when the subjects were allowed to rest. During this time, they were disengaged from the experimental apparatus. Subjects were requested to reach their maximal force in around three seconds and to maintain it for 2 to 4 seconds, under verbal encouragement. Out of the three trials the global maximum value of the low-pass filtered force (4\textsuperscript{th}-order Butterworth filter, 30Hz cut-off frequency) was retained and used for normalization.

\textit{Experimental conditions - }
Three levels of force and three levels of cuff pressure were investigated, the variables were randomly picked to define the 9 experimental test cases for the subject.  
The force levels investigated were \Flow, \Fmid \ and \Fhigh \ of the MVC, and the pressure levels determined as: atmospheric (AP - cuff deflated and valve open), low (LP - the average value between systolic and diastolic pressure) and high pressure (HP - 1.3 times the systolic pressure). 

\textit{Execution of the experimental protocol - }
For each of the 9 investigated cases, the desired level of pressure was applied and maintained, and the recording started. During the AP condition the sphygmomanometer was inflated while the valve was left open in pretence, for blinding. After 30 seconds of baseline, the subjects flexed their elbow with the prescribed level of torque while following a straight ramp (lasting 15 seconds) being presented on the visual feedback monitor. After the ramp, they were asked to hold the same torque for another 15 seconds. Then, they rested for 120 seconds with the cuff inflated. The contraction was repeated three times (reported, from now on, as 'trials' - T1, T2 and T3). At the end of the hold phase, the subjects were asked to report their level of discomfort. Throughout the resting phases, the external pressure was constantly adjusted to maintain the desired level. After the end of T3, the cuff was deflated and the subjects released from the constrains. Each condition was separated by a resting period of 10 minutes or more, to wash out the effects of BFR. The experimental sequence is summarized in figure \ref{fig_exp_sequence}.

{\centering Figure \ref{fig_exp_sequence} about here \par}

\subsection{Data analysis}
\textit{Data segmentation - } 
The central \nseconds seconds of the steady-state contraction were considered for further analysis. The torque and EMG data were processed in non-overlapping windows of \winsamp samples (\winms ms). Where appropriate, for each window one value per variable was computed. 

\textit{Torque data processing - } Segmented torque data was low-pass filtered (10\textsuperscript{th}-order Butterworth filter, with a 3dB frequency of 30Hz) and normalized with respect to the MVC value. The coefficient of variation (CoV) was computed to estimate the variability in the stationary segment of torque signal. The root mean square (RMS) of the torque was calculated for each window (as reported in figure \ref{fig_torque}) and stored for further analyses. 

{\centering Figure \ref{fig_torque} about here \par}

The low-pass filtered torque data was de-trended by removing its mean and and transferring it the frequency domain, the latter being needed to carry out the spectral analyses. The p-Welch's power density spectrum \citep{welch1967} was estimated on non-overlapping windows. To improve spectral resolution, 20480 discrete Fourier transform points were obtained by zero-padding. Finally, a Hamming window of length 2048 (equal to the sampling frequency) was applied. The spectrum was estimated separately for each trial, condition (pressure and level of torque) and subject. A confidence interval of 95 percent was chosen \citep{yavuz2015,rosenberg1989}. The normalized power of the spectrum - calculated as power/sum(power) - was also computed.

Additionally, each spectrum that was obtained from a stationary segment was divided into two frequency bands (1 to 5Hz, and 5 to 15Hz, respectively). The first one ($\delta$-band) is considered to reflect the common drive, the second ($\alpha$-band) is associated with physiological tremor and somatosensory information \citep{myers2004,dai2017}.  For both bands, the area under the curve (AUC\textsubscript{$\delta$} and AUC\textsubscript{$\alpha$}) was calculated with a numerical integration using a  trapezoidal method with a uniform spacing. Lastly, for a direct comparison, the difference of both bands was calculated for each condition as in \ref{eq_AUC}. 
\begin{equation}
AUC = \int_{a}^{b}f(x)dx=\frac{b-a}{2N} \sum_{n=1}^{N}(f(x_n)+f(x_{n+1})). \label{eq_AUC}
\end{equation}
Thereby,  \textit{a} and \textit{b} are the limiting frequencies 1Hz to 5Hz and 5Hz to 15Hz, respectively, and \textit{N} is the number of discretization  intervals (50 for the first, and 100 for the second band, respectively).
The total AUC (AUC\textsubscript{t}) from 1 to 20Hz is computed to estimate variability in torque within the frequency domain.  

\textit{EMG data processing - }
For each trial and each subject a visual inspection of the EMG signal was performed and noisy or flat channels were marked and excluded from further analyses; data was then band-pass filtered (4\textsuperscript{th} order Butterworth band-pass filter between 20 and 450Hz, see \cite{Hermens2000}) and a single differential was obtained along the muscle fibres.
Finally, the RMS and median frequency (MDF) (see \cite{Merletti2004}) values for each channel and one value (see below) of the average muscle fibres conduction velocity (MFCV) were computed.
To estimate the average MFCV, double differential signals were obtained from single differential along the fibres, they were interpolated \ninterp times and, for each pair of signals the time delay value was estimated by means of a cross-correlation based algorithm \citep{farina2004methods,sbriccoli2009non,klotz2019modelling}. The final value of MFCV was chosen as the lowest value across those, whose normalized cross-correlation exceeded a threshold of \CCthreshold.
Torque and EMG RMS data were normalized with respect to the value obtained during MVC. MDF and MFCV data were not normalized. 

To infer the adjustments of the central nervous system to the perturbation over time, each computed variable was fitted with a linear function whose intercept and slope were retained for further analysis \citep{Falla2003fatigue}. 
Finally, to test for changes in the variability of neuromechanical output, the coefficient of variation was calculated. 

\textit{Statistical analysis - }
For each variable and condition, data from all the subjects were pooled together and Shapiro-Wilk tests were applied to test for normality. As data were not normally distributed, a non parametric test was used. Data were summarized using mean and standard deviation.  

The influence of condition on the dependent variables was analysed separately for each torque level using a Kruskal-Wallis rank-sum test. Post-hoc comparisons were performed by means of Dunn’s test, with a Benjamini-Hochberg correction (alpha level set to 0.05/2). Despite the repeated nature of acquisitions, we considered the independent variables as inter-subject factors since they put subjects in different conditions. In particular, the 3 levels of the variable 'trial' were considered diverse as the desired pressure level was maintained across T1, T2 and T3. The results were expected to produce differences between trials for each pressure level.






