clc
% List "state fo health"
% of all subjects(12) for all conditions(9) over the trials(3) 
% Values are manuel implemented

condition = {'AM_15';'AM_30';'AM_50';'LP_15';'LP_30';'LP_50';'HP_15';'HP_30';'HP_50'};
trial = {'Trial_1';'Trial_2';'Trial_3'};
subject_name = {'S001','S002','S003','S004','S005','S006','S007','S008','S009','S010','S011','S012'}';

%S001
Name = 'S001';
Randomization = [4;3;7;9;8;1;5;6;2];
State_of_health = [1,1,3;1,3,7;1.1,1.2,1.3;1,1,1;1,2,2;1,2,2;1,1,1;1,1,2;1,1,1];
S001 = struct('Name',Name,'Randomization',Randomization,'State_of_health',State_of_health);


%S002
Name = 'S002';
Randomization = [5;7;8;3;6;1;2;4;9];
State_of_health = [1,1,1.5;1,1.2,1.5;1,1,1.2;1,1,2;1,1,1;1,1,1.5;1,1,1;1,1.2,1.2;1.2,2,3];
S002 = struct('Name',Name,'Randomization',Randomization,'State_of_health',State_of_health);

%S003
Name = 'S003';
Randomization = [4;9;2;6;5;1;7;8;3];
State_of_health = [2,4,6.5;2,2,2;3.2,7.5,9;1,1,1;1.5,1.6,3;1,1,1;1,1.3,2.2;1.2,3.2,6.4;1.7,1.6,1.6];
S003 = struct('Name',Name,'Randomization',Randomization,'State_of_health',State_of_health);

%S004
Name = 'S003';
Randomization = [9;6;2;1;3;8;4;7;5];
State_of_health = [3,4,3.5;2,2.5,2;3,3.5,4;3.75,4.5,5;3.5,5,4.5;2.5,2,2;3.2,3.3,4;3,3.3,3;2,2,2];
S004 = struct('Name',Name,'Randomization',Randomization,'State_of_health',State_of_health);

%S005
Name = 'S005';
Randomization = [1;6;8;4;2;9;5;7;3];
State_of_health = [2,1.5,1.5;2.5,2.5,3.5;3,3.5,4.5;2,3,3;2,2.5,3.5;1,1,1;2,3,4.5;1,1.5,1.5;1.5,3,3.5];
S005 = struct('Name',Name,'Randomization',Randomization,'State_of_health',State_of_health);

%S006
Name = 'S006';
Randomization = [5;6;9;7;4;1;8;2;3];
State_of_health = [2,3,4;3,4,6;3,5,6;3,4,5;2,2,2;3,4,4;4,4,5;4,5.5,6;2,2,3;3,4,5];
S006 = struct('Name',Name,'Randomization',Randomization,'State_of_health',State_of_health);

%S007
Name = 'S007';
Randomization = [5;2;6;4;9;8;7;1;3];
State_of_health = [5,5,6;2,2,3;3,5,7;2,3,3;2,2,2;3,3,3;3,3,4;4,5,6;3,4,4];
S007 = struct('Name',Name,'Randomization',Randomization,'State_of_health',State_of_health);

%S008
Name = 'S008';
Randomization = [5;8;3;9;2;7;6;1;4];
State_of_health = [2.5,5,6;1,5,7;2,2,3;3,5,10;1,1,2;2,3,4;4,6,8;2,2,2.5;2,5.5,7];
S008 = struct('Name',Name,'Randomization',Randomization,'State_of_health',State_of_health);

%S009
Name = 'S009';
Randomization = [9;2;1;6;8;3;7;4;5];
State_of_health = [1,1,2;3,3,2;3,5,4;2,2,4;3,6,7;1,1,1;1,1,1;2,2,3;1,1,1];
S009 = struct('Name',Name,'Randomization',Randomization,'State_of_health',State_of_health);

%S010
Name = 'S010';
Randomization = [2;6;8;4;1;5;7;9;3];
State_of_health = [3,3.5,3.5;1,1,1.5;2.5,3.5,5.5;1.5,2,4;2,3,4;1.5,2,2;1.5,2.5,3.5;1.5,2,2;2.5,3.5,5];
S010 = struct('Name',Name,'Randomization',Randomization,'State_of_health',State_of_health);

%S011
Name = 'S011';
Randomization = [4;7;5;3;9;1;2;8;6];
State_of_health = [7,8,8;6.5,9,9;5,7,7;3,3,2;5,4,4;7,8.5,9;4,6,7;7,9,9.5;7,8,8.5];
S011 = struct('Name',Name,'Randomization',Randomization,'State_of_health',State_of_health);

%S012
Name = 'S012';
Randomization = [4;9;3;8;5;2;7;6;1];
State_of_health = [1,1.25,4;1,2,2.5;1,1,1;1,1,1;1,1.2,2.5;1,2,3.5;1,1.5,2;1,1.2,1.1;1,1,1];
S012 = struct('Name',Name,'Randomization',Randomization,'State_of_health',State_of_health);


S_all = struct('S001',S001,'S002',S002,'S003',S003,'S004',S004,'S005',S005,'S006',S006,'S007',S007,'S008',S008,...
    'S009',S009,'S010',S010,'S011',S011,'S012',S012);


%% Ordered Values from AM 15 to HP 50

subjects = fieldnames(S_all);

subject_data = zeros(0,0,0); %3D Matrix with 1=subjects(12), 2=conditions(9), 3=trials(3)

for i = 1:length(subjects)
    
    Randomization = S_all.(subjects{i}).Randomization;
    sorted = S_all.(subjects{i}).State_of_health(Randomization,:);
    S_all.(subjects{i}).Sorted = sorted;
    
    subject_data(i,:,:) = sorted;
    
end

%mat = permute(mat, [3 2 1]); %Reihenfolge der Dimensionen ver?ndern

%% average and std over first dimension, i.e. subjects
subject_data_average = mean(subject_data,1) ;
subject_data_average = squeeze(subject_data_average); %Remove singleton dimensions

subject_data_std = std(subject_data,0,1);
subject_data_std = squeeze(subject_data_std); %Remove singleton dimensions

Table_mean = array2table(subject_data_average,'RowNames',condition,'VariableNames',trial); %Tabelle benannt
Table_std = array2table(subject_data_std,'RowNames',condition,'VariableNames',trial);

%% Write table
writetable(Table_mean,'table_mean_state_of_health.csv'); %table mean
writetable(Table_std,'table_std_state_of_health.csv'); %table std
%% Excel file for statistics
header  = {'Discomfort','Subject', 'Pressure','Torque','Trial'};
condition = {'AM_15';'AM_30';'AM_50';'LP_15';'LP_30';'LP_50';'HP_15';'HP_30';'HP_50'};
torque = {1;2;3;1;2;3;1;2;3};
pressure = {1;1;1;2;2;2;3;3;3};

Ns = size(subject_data,1); %subject
Nc = size(subject_data,2); %condition
Nt = size(subject_data,3); %trial
data = [];
for nsubj = 1:Ns
    for ncond = 1:Nc
        for ntrial = 1:Nt
            temp = [subject_data(nsubj,ncond,ntrial), nsubj, pressure(ncond), torque(ncond), ntrial];
            data = [data; temp];
        end
    end
end
out_name = 'C:\Repos_temp\premg\2018_Effect_of_BFR_on_sEMG\manuscript\tables\Conditions subjects\20200213_discomfort_data.xls';
xlswrite(out_name, header, 'sheet1','A1');
xlswrite(out_name, data, 'sheet1','A2');

