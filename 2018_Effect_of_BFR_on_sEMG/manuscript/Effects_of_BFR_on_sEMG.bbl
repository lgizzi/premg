\begin{thebibliography}{55}
\providecommand{\natexlab}[1]{#1}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi:\discretionary{}{}{}#1}\else
  \providecommand{\doi}{doi:\discretionary{}{}{}\begingroup
  \urlstyle{rm}\Url}\fi
\providecommand{\selectlanguage}[1]{\relax}
\providecommand{\bibAnnoteFile}[1]{%
  \IfFileExists{#1}{\begin{quotation}\noindent\textsc{Key:} #1\\
  \textsc{Annotation:}\ \input{#1}\end{quotation}}{}}
\providecommand{\bibAnnote}[2]{%
  \begin{quotation}\noindent\textsc{Key:} #1\\
  \textsc{Annotation:}\ #2\end{quotation}}

\bibitem[{Abe et~al.(2012)Abe, Loenneke, Fahs, Rossow, Thiebaud, and
  Bemben}]{Abe2012}
Abe, T., Loenneke, J.~P., Fahs, C.~A., Rossow, L.~M., Thiebaud, R.~S., and
  Bemben, M.~G. (2012).
\newblock {Exercise intensity and muscle hypertrophy in blood flow-restricted
  limbs and non-restricted muscles: a brief review.}
\newblock \emph{Clinical physiology and functional imaging} 32, 247--52.
\newblock \doi{10.1111/j.1475-097X.2012.01126.x}
\bibAnnoteFile{Abe2012}

\bibitem[{Amann et~al.(2009)Amann, Proctor, Sebranek, Pegelow, and
  Dempsey}]{amann2009opioid}
Amann, M., Proctor, L.~T., Sebranek, J.~J., Pegelow, D.~F., and Dempsey, J.~A.
  (2009).
\newblock Opioid-mediated muscle afferents inhibit central motor drive and
  limit peripheral muscle fatigue development in humans.
\newblock \emph{The Journal of physiology} 587, 271--283
\bibAnnoteFile{amann2009opioid}

\bibitem[{Blain and Hureau(2017)}]{blain2017limitation}
Blain, G.~M. and Hureau, T.~J. (2017).
\newblock Limitation of fatigue and performance during exercise: the
  brain--muscle interaction.
\newblock \emph{Experimental physiology} 102, 3--4
\bibAnnoteFile{blain2017limitation}

\bibitem[{Christakos et~al.(2006)Christakos, Papadimitriou, and
  Erimaki}]{christakos2006}
Christakos, C.~N., Papadimitriou, N.~A., and Erimaki, S. (2006).
\newblock Parallel neuronal mechanisms underlying physiological force tremor in
  steady muscle contractions of humans.
\newblock \emph{Journal of neurophysiology} 95, 53--66
\bibAnnoteFile{christakos2006}

\bibitem[{Dai et~al.(2017)Dai, Suresh, Suresh, Rymer, and Hu}]{dai2017}
Dai, C., Suresh, N.~L., Suresh, A.~K., Rymer, W.~Z., and Hu, X. (2017).
\newblock Altered motor unit discharge coherence in paretic muscles of stroke
  survivors.
\newblock \emph{Frontiers in neurology} 8, 202
\bibAnnoteFile{dai2017}

\bibitem[{Dankel et~al.(2018)Dankel, Buckner, Jessee, Mattocks, Mouser, Counts
  et~al.}]{dankel2018}
Dankel, S.~J., Buckner, S.~L., Jessee, M.~B., Mattocks, K.~T., Mouser, J.~G.,
  Counts, B.~R., et~al. (2018).
\newblock Can blood flow restriction augment muscle activation during high-load
  training?
\newblock \emph{Clinical physiology and functional imaging} 38, 291--295
\bibAnnoteFile{dankel2018}

\bibitem[{De~Luca et~al.(1982)De~Luca, LeFever, McCue, and
  Xenakis}]{deLuca1982}
De~Luca, C., LeFever, R., McCue, M., and Xenakis, A. (1982).
\newblock Control scheme governing concurrently active human motor units during
  voluntary contractions.
\newblock \emph{The Journal of physiology} 329, 129--142
\bibAnnoteFile{deLuca1982}

\bibitem[{Erimaki and Christakos(2008)}]{erimaki2008coherent}
Erimaki, S. and Christakos, C.~N. (2008).
\newblock Coherent motor unit rhythms in the 6--10 hz range during time-varying
  voluntary muscle contractions: neural mechanism and relation to rhythmical
  motor control.
\newblock \emph{Journal of neurophysiology} 99, 473--483
\bibAnnoteFile{erimaki2008coherent}

\bibitem[{Falla et~al.(2003)Falla, Rainoldi, Merletti, and
  Jull}]{Falla2003fatigue}
Falla, D., Rainoldi, A., Merletti, R., and Jull, G. (2003).
\newblock Myoelectric manifestations of sternocleidomastoid and anterior
  scalene muscle fatigue in chronic neck pain patients.
\newblock \emph{Clinical Neurophysiology} 114, 488 -- 495.
\newblock \doi{https://doi.org/10.1016/S1388-2457(02)00418-2}
\bibAnnoteFile{Falla2003fatigue}

\bibitem[{Farina and Merletti(2004)}]{farina2004methods}
Farina, D. and Merletti, R. (2004).
\newblock Methods for estimating muscle fibre conduction velocity from surface
  electromyographic signals.
\newblock \emph{Medical and biological Engineering and Computing} 42, 432--445
\bibAnnoteFile{farina2004methods}

\bibitem[{Farina et~al.(2012)Farina, Negro, Gizzi, and Falla}]{farina2012}
Farina, D., Negro, F., Gizzi, L., and Falla, D. (2012).
\newblock Low-frequency oscillations of the neural drive to the muscle are
  increased with experimental muscle pain.
\newblock \emph{Journal of neurophysiology} 107, 958--965
\bibAnnoteFile{farina2012}

\bibitem[{Fatela et~al.(2019)Fatela, Mendonca, Veloso, Avela, and
  Mil-Homens}]{fatela2019}
Fatela, P., Mendonca, G.~V., Veloso, A.~P., Avela, J., and Mil-Homens, P.
  (2019).
\newblock Blood flow restriction alters motor unit behavior during resistance
  exercise.
\newblock \emph{International journal of sports medicine} 40, 555--562
\bibAnnoteFile{fatela2019}

\bibitem[{Fry et~al.(2010)Fry, Glynn, Drummond, Timmerman, Fujita, Abe
  et~al.}]{Fry2010}
Fry, C.~S., Glynn, E.~L., Drummond, M.~J., Timmerman, K.~L., Fujita, S., Abe,
  T., et~al. (2010).
\newblock {Blood flow restriction exercise stimulates mTORC1 signaling and
  muscle protein synthesis in older men}.
\newblock \emph{Journal of Applied Physiology} 108, 1199--1209.
\newblock \doi{10.1152/japplphysiol.01266.2009}
\bibAnnoteFile{Fry2010}

\bibitem[{Fujita et~al.(2007)Fujita, Abe, Drummond, Cadenas, Dreyer, Sato
  et~al.}]{Fujita2007}
Fujita, S., Abe, T., Drummond, M.~J., Cadenas, J.~G., Dreyer, H.~C., Sato, Y.,
  et~al. (2007).
\newblock {Blood flow restriction during low-intensity resistance exercise
  increases S6K1 phosphorylation and muscle protein synthesis}.
\newblock \emph{Journal of Applied Physiology} 103, 903--910.
\newblock \doi{10.1152/japplphysiol.00195.2007}
\bibAnnoteFile{Fujita2007}

\bibitem[{Grey et~al.(2001)Grey, Ladouceur, Andersen, Nielsen, and
  Sinkj{\ae}r}]{grey2001}
Grey, M.~J., Ladouceur, M., Andersen, J.~B., Nielsen, J.~B., and Sinkj{\ae}r,
  T. (2001).
\newblock Group ii muscle afferents probably contribute to the medium latency
  soleus stretch reflex during walking in humans.
\newblock \emph{The Journal of physiology} 534, 925--933
\bibAnnoteFile{grey2001}

\bibitem[{Gundermann et~al.(2012)Gundermann, Fry, Dickinson, Walker, Timmerman,
  Drummond et~al.}]{Gundermann2012}
Gundermann, D.~M., Fry, C.~S., Dickinson, J.~M., Walker, D.~K., Timmerman,
  K.~L., Drummond, M.~J., et~al. (2012).
\newblock {Reactive hyperemia is not responsible for stimulating muscle protein
  synthesis following blood flow restriction exercise}.
\newblock \emph{Journal of Applied Physiology} 112, 1520--1528.
\newblock \doi{10.1152/japplphysiol.01267.2011}
\bibAnnoteFile{Gundermann2012}

\bibitem[{Halliday and Redfearn(1958)}]{halliday1958finger}
Halliday, A. and Redfearn, J. (1958).
\newblock Finger tremor in tabetic patients and its bearing on the mechanism
  producing the rhythm of physiological tremor.
\newblock \emph{Journal of neurology, neurosurgery, and psychiatry} 21, 101
\bibAnnoteFile{halliday1958finger}

\bibitem[{Heidlauf et~al.(2016)Heidlauf, Klotz, Rode, Altan, Bleiler, Siebert
  et~al.}]{heidlauf2016multi}
Heidlauf, T., Klotz, T., Rode, C., Altan, E., Bleiler, C., Siebert, T., et~al.
  (2016).
\newblock A multi-scale continuum model of skeletal muscle mechanics predicting
  force enhancement based on actin--titin interaction.
\newblock \emph{Biomechanics and modeling in mechanobiology} 15, 1423--1437
\bibAnnoteFile{heidlauf2016multi}

\bibitem[{Hermens et~al.(2000)Hermens, Freriks, Disselhorst-Klug, and
  Rau}]{Hermens2000}
Hermens, H.~J., Freriks, B., Disselhorst-Klug, C., and Rau, G. (2000).
\newblock {Development of recommendations for SEMG sensors and sensor placement
  procedures.}
\newblock \emph{Journal of electromyography and kinesiology : official journal
  of the International Society of Electrophysiological Kinesiology} 10,
  361--74.
\newblock \doi{10.1016/S1050-6411(00)00027-4}
\bibAnnoteFile{Hermens2000}

\bibitem[{Hughes et~al.(2017)Hughes, Paton, Rosenblatt, Gissane, and
  Patterson}]{hughes2017}
Hughes, L., Paton, B., Rosenblatt, B., Gissane, C., and Patterson, S.~D.
  (2017).
\newblock Blood flow restriction training in clinical musculoskeletal
  rehabilitation: a systematic review and meta-analysis.
\newblock \emph{Br J Sports Med} 51, 1003--1011
\bibAnnoteFile{hughes2017}

\bibitem[{Husmann et~al.(2018)Husmann, Mittlmeier, Bruhn, Zschorlich, and
  Behrens}]{husmann2018}
Husmann, F., Mittlmeier, T., Bruhn, S., Zschorlich, V., and Behrens, M. (2018).
\newblock Impact of blood flow restriction exercise on muscle fatigue
  development and recovery.
\newblock \emph{Med Sci Sports Exerc} 50, 436--446
\bibAnnoteFile{husmann2018}

\bibitem[{Hyngstrom et~al.(2018)Hyngstrom, Murphy, Nguyen, Schmit, Negro,
  Gutterman et~al.}]{hyngstrom2018}
Hyngstrom, A.~S., Murphy, S.~A., Nguyen, J., Schmit, B.~D., Negro, F.,
  Gutterman, D.~D., et~al. (2018).
\newblock Ischemic conditioning increases strength and volitional activation of
  paretic muscle in chronic stroke: a pilot study.
\newblock \emph{Journal of Applied Physiology} 124, 1140--1147
\bibAnnoteFile{hyngstrom2018}

\bibitem[{Ishizaka et~al.(2019)Ishizaka, Uematsu, Mizushima, Nozawa,
  Katayanagi, Matsumoto et~al.}]{ishizaka2019}
Ishizaka, H., Uematsu, A., Mizushima, Y., Nozawa, N., Katayanagi, S.,
  Matsumoto, K., et~al. (2019).
\newblock Blood flow restriction increases the neural activation of the knee
  extensors during very low-intensity leg extension exercise in cardiovascular
  patients: A pilot study.
\newblock \emph{Journal of clinical medicine} 8, 1252
\bibAnnoteFile{ishizaka2019}

\bibitem[{Klotz et~al.(2019)Klotz, Gizzi, Yavuz, and
  R{\"o}hrle}]{klotz2019modelling}
Klotz, T., Gizzi, L., Yavuz, U.~{\c{S}}., and R{\"o}hrle, O. (2019).
\newblock Modelling the electrical activity of skeletal muscle tissue using a
  multi-domain approach.
\newblock \emph{Biomechanics and modeling in mechanobiology} , 1--15
\bibAnnoteFile{klotz2019modelling}

\bibitem[{Koch et~al.(2020)Koch, Schneider, Helmig, and Jenny}]{Koch2020}
Koch, T., Schneider, M., Helmig, R., and Jenny, P. (2020).
\newblock Modeling tissue perfusion in terms of 1d-3d embedded mixed-dimension
  coupled problems with distributed sources.
\newblock \emph{Journal of Computational Physics} 410, 109370.
\newblock \doi{https://doi.org/10.1016/j.jcp.2020.109370}
\bibAnnoteFile{Koch2020}

\bibitem[{Laine et~al.(2014)Laine, Yavuz, and Farina}]{laine2014task}
Laine, C., Yavuz, {\c{S}}., and Farina, D. (2014).
\newblock Task-related changes in sensorimotor integration influence the common
  synaptic input to motor neurones.
\newblock \emph{Acta Physiologica} 211, 229--239
\bibAnnoteFile{laine2014task}

\bibitem[{Laine et~al.(2013)Laine, Negro, and Farina}]{laine2013neural}
Laine, C.~M., Negro, F., and Farina, D. (2013).
\newblock Neural correlates of task-related changes in physiological tremor.
\newblock \emph{Journal of neurophysiology} 110, 170--176
\bibAnnoteFile{laine2013neural}

\bibitem[{Lippold(1970)}]{lippold1970oscillation}
Lippold, O.~C. (1970).
\newblock Oscillation in the stretch reflex arc and the origin of the
  rhythmical, 8--12 c/s component of physiological tremor.
\newblock \emph{The Journal of physiology} 206, 359--382
\bibAnnoteFile{lippold1970oscillation}

\bibitem[{Loenneke et~al.(2012{\natexlab{a}})Loenneke, Abe, Wilson,
  Ugrinowitsch, and Bemben}]{Loenneke2012}
Loenneke, J.~P., Abe, T., Wilson, J.~M., Ugrinowitsch, C., and Bemben, M.~G.
  (2012{\natexlab{a}}).
\newblock {Blood flow restriction: how does it work?}
\newblock \emph{Frontiers in physiology} 3, 392.
\newblock \doi{10.3389/fphys.2012.00392}
\bibAnnoteFile{Loenneke2012}

\bibitem[{Loenneke et~al.(2015)Loenneke, Kim, Fahs, Thiebaud, Abe, Larson
  et~al.}]{loenneke2015}
Loenneke, J.~P., Kim, D., Fahs, C.~A., Thiebaud, R.~S., Abe, T., Larson, R.~D.,
  et~al. (2015).
\newblock Effects of exercise with and without different degrees of blood flow
  restriction on torque and muscle activation.
\newblock \emph{Muscle \& nerve} 51, 713--721
\bibAnnoteFile{loenneke2015}

\bibitem[{Loenneke et~al.(2012{\natexlab{b}})Loenneke, Wilson, Mar{\'{i}}n,
  Zourdos, and Bemben}]{Loenneke2012a}
Loenneke, J.~P., Wilson, J.~M., Mar{\'{i}}n, P.~J., Zourdos, M.~C., and Bemben,
  M.~G. (2012{\natexlab{b}}).
\newblock {Low intensity blood flow restriction training: a meta-analysis.}
\newblock \emph{European journal of applied physiology} 112, 1849--59.
\newblock \doi{10.1007/s00421-011-2167-x}
\bibAnnoteFile{Loenneke2012a}

\bibitem[{Manini and Clark(2009)}]{manini2009}
Manini, T.~M. and Clark, B.~C. (2009).
\newblock Blood flow restricted exercise and skeletal muscle health.
\newblock \emph{Exercise and sport sciences reviews} 37, 78--85
\bibAnnoteFile{manini2009}

\bibitem[{Mauritz and Dietz(1980)}]{Mauritz1980}
Mauritz, K.~H. and Dietz, V. (1980).
\newblock {Characteristics of postural instability induced by ischemic blocking
  of leg afferents}.
\newblock \emph{Experimental Brain Research} 38, 117--119.
\newblock \doi{10.1007/BF00237939}
\bibAnnoteFile{Mauritz1980}

\bibitem[{Merletti et~al.(2004)Merletti, Parker, and Parker}]{Merletti2004}
Merletti, R., Parker, P.~A., and Parker, P.~J. (2004).
\newblock \emph{Electromyography: physiology, engineering, and non-invasive
  applications}, vol.~11 (John Wiley \& Sons)
\bibAnnoteFile{Merletti2004}

\bibitem[{Merletti et~al.(1984)Merletti, Sabbahi, and {De Luca}}]{Merletti1984}
Merletti, R., Sabbahi, M.~A., and {De Luca}, C.~J. (1984).
\newblock {Median frequency of the myoelectric signal. Effects of muscle
  ischemia and cooling}.
\newblock \emph{Eur J Appl Physiol} 52, 258--265
\bibAnnoteFile{Merletti1984}

\bibitem[{Murphy et~al.(2019)Murphy, Durand, Negro, Farina, Hunter, Schmit
  et~al.}]{murphy2019}
Murphy, S., Durand, M., Negro, F., Farina, D., Hunter, S., Schmit, B., et~al.
  (2019).
\newblock The relationship between blood flow and motor unit firing rates in
  response to fatiguing exercise post-stroke.
\newblock \emph{Frontiers in physiology} 10
\bibAnnoteFile{murphy2019}

\bibitem[{Myers et~al.(2004)Myers, Erim, and Lowery}]{myers2004}
Myers, L.~J., Erim, Z., and Lowery, M.~M. (2004).
\newblock Time and frequency domain methods for quantifying common modulation
  of motor unit firing patterns.
\newblock \emph{Journal of neuroengineering and rehabilitation} 1, 2
\bibAnnoteFile{myers2004}

\bibitem[{R{\"o}hrle et~al.(2017)R{\"o}hrle, Sprenger, and
  Schmitt}]{Roehrle2017}
R{\"o}hrle, O., Sprenger, M., and Schmitt, S. (2017).
\newblock A two-muscle, continuum-mechanical forward simulation of the upper
  limb.
\newblock \emph{Biomechanics and modeling in mechanobiology} 16, 743--762
\bibAnnoteFile{Roehrle2017}

\bibitem[{R{\"o}hrle et~al.(2019)R{\"o}hrle, Yavuz, Klotz, Negro, and
  Heidlauf}]{Roehrle2019}
R{\"o}hrle, O., Yavuz, U.~{\c{S}}., Klotz, T., Negro, F., and Heidlauf, T.
  (2019).
\newblock Multiscale modeling of the neuromuscular system: Coupling
  neurophysiology and skeletal muscle mechanics.
\newblock \emph{Wiley Interdisciplinary Reviews: Systems Biology and Medicine}
  11, e1457
\bibAnnoteFile{Roehrle2019}

\bibitem[{Rosenberg et~al.(1989)Rosenberg, Amjad, Breeze, Brillinger, and
  Halliday}]{rosenberg1989}
Rosenberg, J., Amjad, A., Breeze, P., Brillinger, D., and Halliday, D. (1989).
\newblock The fourier approach to the identification of functional coupling
  between neuronal spike trains.
\newblock \emph{Progress in biophysics and molecular biology} 53, 1--31
\bibAnnoteFile{rosenberg1989}

\bibitem[{Sbriccoli et~al.(2009)Sbriccoli, Sacchetti, Felici, Gizzi, Lenti,
  Scotto et~al.}]{sbriccoli2009non}
Sbriccoli, P., Sacchetti, M., Felici, F., Gizzi, L., Lenti, M., Scotto, A.,
  et~al. (2009).
\newblock Non-invasive assessment of muscle fiber conduction velocity during an
  incremental maximal cycling test.
\newblock \emph{Journal of Electromyography and Kinesiology} 19, e380--e386
\bibAnnoteFile{sbriccoli2009non}

\bibitem[{Schmid et~al.(2019)Schmid, Klotz, Siebert, and
  R{\"o}hrle}]{schmid2019characterization}
Schmid, L., Klotz, T., Siebert, T., and R{\"o}hrle, O. (2019).
\newblock Characterization of electromechanical delay based on a biophysical
  multi-scale skeletal muscle model.
\newblock \emph{Frontiers in physiology} 10, 1270
\bibAnnoteFile{schmid2019characterization}

\bibitem[{Sharma et~al.(2015)Sharma, Marsh, Cunniffe, Cardinale, Yellon, and
  Davidson}]{sharma2015}
Sharma, V., Marsh, R., Cunniffe, B., Cardinale, M., Yellon, D.~M., and
  Davidson, S.~M. (2015).
\newblock From protecting the heart to improving athletic performance--the
  benefits of local and remote ischaemic preconditioning.
\newblock \emph{Cardiovascular drugs and therapy} 29, 573--588
\bibAnnoteFile{sharma2015}

\bibitem[{Sousa et~al.(2017)Sousa, Neto, Santos, Ara{\'{u}}jo, Silva, and
  Cirilo-Sousa}]{Sousa2017}
Sousa, J., Neto, G.~R., Santos, H.~H., Ara{\'{u}}jo, J.~P., Silva, H.~G., and
  Cirilo-Sousa, M.~S. (2017).
\newblock {Effects of strength training with blood flow restriction on torque,
  muscle activation and local muscular endurance in healthy subjects.}
\newblock \emph{Biology of sport} 34, 83--90.
\newblock \doi{10.5114/biolsport.2017.63738}
\bibAnnoteFile{Sousa2017}

\bibitem[{Takarada et~al.(2000)Takarada, Takazawa, Sato, Takebayashi, Tanaka,
  and Ishii}]{takarada2000effects}
Takarada, Y., Takazawa, H., Sato, Y., Takebayashi, S., Tanaka, Y., and Ishii,
  N. (2000).
\newblock Effects of resistance exercise combined with moderate vascular
  occlusion on muscular function in humans.
\newblock \emph{Journal of applied physiology} 88, 2097--2106
\bibAnnoteFile{takarada2000effects}

\bibitem[{Taylor et~al.(2016)Taylor, Amann, Duchateau, Meeusen, and
  Rice}]{taylor2016}
Taylor, J.~L., Amann, M., Duchateau, J., Meeusen, R., and Rice, C.~L. (2016).
\newblock Neural contributions to muscle fatigue: from the brain to the muscle
  and back again.
\newblock \emph{Medicine and science in sports and exercise} 48, 2294
\bibAnnoteFile{taylor2016}

\bibitem[{Teixeira et~al.(2018)Teixeira, Barroso, Silva-Batista, Laurentino,
  Loenneke, Roschel et~al.}]{teixeira2018}
Teixeira, E.~L., Barroso, R., Silva-Batista, C., Laurentino, G.~C., Loenneke,
  J.~P., Roschel, H., et~al. (2018).
\newblock Blood flow restriction increases metabolic stress but decreases
  muscle activation during high-load resistance exercise.
\newblock \emph{Muscle \& nerve} 57, 107--111
\bibAnnoteFile{teixeira2018}

\bibitem[{Thiebaud et~al.(2013)Thiebaud, Yasuda, Loenneke, and
  Abe}]{Thiebaud2013}
Thiebaud, R.~S., Yasuda, T., Loenneke, J.~P., and Abe, T. (2013).
\newblock {Effects of low-intensity concentric and eccentric exercise combined
  with blood flow restriction on indices of exercise-induced muscle damage.}
\newblock \emph{Interventional medicine {\&} applied science} 5, 53--9.
\newblock \doi{10.1556/IMAS.5.2013.2.1}
\bibAnnoteFile{Thiebaud2013}

\bibitem[{Valentin et~al.(2018)Valentin, Sprenger, Pfl{\"u}ger, and
  R{\"o}hrle}]{Valentin2018}
Valentin, J., Sprenger, M., Pfl{\"u}ger, D., and R{\"o}hrle, O. (2018).
\newblock Gradient-based optimization with b-splines on sparse grids for
  solving forward-dynamics simulations of three-dimensional,
  continuum-mechanical musculoskeletal system models.
\newblock \emph{International journal for numerical methods in biomedical
  engineering} 34, e2965
\bibAnnoteFile{Valentin2018}

\bibitem[{Welch(1967)}]{welch1967}
Welch, P. (1967).
\newblock The use of fast fourier transform for the estimation of power
  spectra: a method based on time averaging over short, modified periodograms.
\newblock \emph{IEEE Transactions on audio and electroacoustics} 15, 70--73
\bibAnnoteFile{welch1967}

\bibitem[{Williamson and Hoggart(2005)}]{williamson2005pain}
Williamson, A. and Hoggart, B. (2005).
\newblock Pain: a review of three commonly used pain rating scales.
\newblock \emph{Journal of clinical nursing} 14, 798--804
\bibAnnoteFile{williamson2005pain}

\bibitem[{Yanagisawa and Sanomura(2017)}]{Yanagisawa2017}
Yanagisawa, O. and Sanomura, M. (2017).
\newblock {Effects of low-load resistance exercise with blood flow restriction
  on high-energy phosphate metabolism and oxygenation level in skeletal
  muscle}.
\newblock \emph{Interventional Medicine and Applied Science} 9, 67--75.
\newblock \doi{10.1556/1646.9.2017.16}
\bibAnnoteFile{Yanagisawa2017}

\bibitem[{Yasuda et~al.(2014)Yasuda, Fukumura, Fukuda, Iida, Imuta, Sato
  et~al.}]{Yasuda2014}
Yasuda, T., Fukumura, K., Fukuda, T., Iida, H., Imuta, H., Sato, Y., et~al.
  (2014).
\newblock {Effects of low-intensity, elastic band resistance exercise combined
  with blood flow restriction on muscle activation.}
\newblock \emph{Scandinavian journal of medicine {\&} science in sports} 24,
  55--61.
\newblock \doi{10.1111/j.1600-0838.2012.01489.x}
\bibAnnoteFile{Yasuda2014}

\bibitem[{Yasuda et~al.(2013)Yasuda, Loenneke, Ogasawara, and Abe}]{Yasuda2013}
Yasuda, T., Loenneke, J.~P., Ogasawara, R., and Abe, T. (2013).
\newblock {Influence of continuous or intermittent blood flow restriction on
  muscle activation during low-intensity multiple sets of resistance exercise.}
\newblock \emph{Acta physiologica Hungarica} 100, 419--26.
\newblock \doi{10.1556/APhysiol.100.2013.4.6}
\bibAnnoteFile{Yasuda2013}

\bibitem[{Yavuz et~al.(2015)Yavuz, Negro, Falla, and Farina}]{yavuz2015}
Yavuz, U.~{\c{S}}., Negro, F., Falla, D., and Farina, D. (2015).
\newblock Experimental muscle pain increases variability of neural drive to
  muscle and decreases motor unit coherence in tremor frequency band.
\newblock \emph{Journal of neurophysiology} 114, 1041--1047
\bibAnnoteFile{yavuz2015}

\end{thebibliography}
