function writeLogFile(errorcode,subjectLabel,currentIteration)
% This function writes in a txt file as log, whenever there is a occasion 
% which is defined to be reported as 'history' of processing.
% All occasion reports of one subject (subjectLabel) will be written into 
% the same txt file.
%
% Code by: Dominic Hillerkuss, 19th December 2019
%
%
% Example Inputs:
% writeLogFile(101,'testLabel',1)
% writeLogFile('Out of interval','S001')
%
%
% OUTPUT:
% txt-file which will be storred in parent directory (at the moment)
% Full filename format of the txt: <DD-MMM-YYYY>_<SubjectLabel>_log.txt
%

switch nargin
    case 0
        disp('ERROR: No arguments found.')
        keyboard
    case 1
        disp('ERROR: Too less arguments found.')
        keyboard
    case 2
        disp('The current iteration number is not defined in the argument and hence will be neglected.')
        currentIteration = 'VOID';
end

%%

disp('HINT: This occasion will be preserved into a history log-file...');
pause(1)

% The parent directory storage might need to be adjusted depending on the automatic
% routines ...
pwdtmp = pwd;
disp('...going to parent directory for storage (this can be adjusted)...')
cd ..  % This can be adjusted! ###

description = ['Error code: ',num2str(errorcode),';  In path: ',pwdtmp,';  Filename or Subject: ',subjectLabel, ';  Iteration: ',num2str(currentIteration)];                
fid = fopen(fullfile(pwd, join([date,'_',subjectLabel,'_log.txt'])), 'a');
if fid == -1, error('ERROR: Can not open or find log file.'); keyboard; end
fprintf(fid, '%s: %s\n', datestr(now, 0), description);
% Reporting line format: DD-MMM-YYYY HH:MM:SS: Error code: <errorcode>; In path: <pwdtmp>; Filename or Subject: <SubjectLabel>; Iteration: <currentIteration>
fclose(fid);
fprintf('\n')
disp(description)
fprintf('\n')
cd(pwdtmp)
disp('Entry into log-file done.')
end

