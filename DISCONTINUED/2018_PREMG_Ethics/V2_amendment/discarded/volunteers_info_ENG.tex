 \bigskip

\centering \LARGE \inserttitle \normalsize 

\refpersonboxeng
\bigskip

\begin{itemize}
	\item Introduction
	
	With the term "Motor control" we refer to the strategies that the central nervous system adopts to perform coordinated movement by means of muscles contraction.
	Muscles contract thanks to small electrical impulses that come from a specific population of neurons (motor neurons) and while contracting generate small electrical signals that can be recorded with surface electrodes (electromyography - EMG) placed on the skin.
	EMG can be used to control orthoses and prostheses, biofeedback applications and so on and is getting more and more popular over time.
	
	In real-life situations, however, the pressure of the electrodes against the skin may change and affect the quality of recorded signal. \textbf{Our first aim in this project is to investigate the effect of external pressure on the quality of EMG signal}.
	
	Applying external pressure causes a temporary restriction of the blood flow, which can have transitory or more long lasting consequences (as an example, blood flow restriction is getting popular, lately, for allowing a faster growth of the muscle mass and its strength while using considerably smaller weights compared to traditional training).
	
	Temporary blood flow restriction causes a series of effects on the muscle fibres (and, indirectly on the motor neurons that controls them) as well as on the muscle and skin sensory organs. The control of individual motor neurons is investigated through EMG and, sometimes, by delivering small electrical stimulations to the muscle, in order to elicit specific patterns of activity of the lower motor neurons.
	\textbf{The second aim of this project is to identify the changes in the strategies that the central nervous systems adopts in case of transitory blood flow restriction. }\\[.5cm]
	
	
	\item Procedures (and possible adverse effects)
	
	Considering preparation, execution and equipment removal, the experiment may last \textbf{up to three hours} (but you will spend most of the time resting between trials). Here is a list of the equipment we will use during the experiments:\\[.5cm]
%	\begin{itemize}
	\subitem - Blood pressure cuff
	
%	\end{itemize}
	An increase in external pressure on the muscle is achieved through a standard pressure measurement cuff (i.e.,sold in pharmacies), which we will use to measure your blood pressure first, and then to create a either partial or total occlusion of your vessels. The cuff will be kept on your limb (either lower or upper) for the entire duration of the experiment, but most of the time it will be deflated. During recordings (which may last at maximum a few minutes), it will be inflated either half the way between your systolic and diastolic pressure, or at \highpressure of your systolic pressure. 
	In certain subjects it can happen that small (i.e. the size of the section of a needle) red dots may appear on the skin during or shortly after the experiment. They are called \textit{petechiae} and are the result of micro-fractures in very superficial capillary vessels. It is completely harmless and painless, yet the dots may stay for a few days on your skin. Some other subjects may experience a transitory numbness of the limb, again this effect will disappear within minutes.\\
	\amend{In very rare cases, blood flow restriction can cause dizziness and light headedness (vasovagal episode). This is comparable with the feeling of stepping too fast out of a chair. It is harmless and the effects are mild, and disappear within a few minutes. }\\ [.5cm]
	
	
	\subitem - High-density EMG
	
	High-density EMG is used to record the electrical signal from your muscle. It consists of an adhesive array where a considerable amount of small electrodes (up to 128) sit. Prior to electrode placement, your skin will be gently abraded and cleaned with surgical alcohol. Afterwards, the matrix will be placed on your skin and secured with surgical tape.
	High-density EMG is not a painful procedure, yet very few subjects may find the removal of the electrodes unpleasant, and people with extremely sensitive skin may report light marks which may stay on the skin for up to a few days. \\[.5cm]
	
	\subitem - Force measurement and visual feedback
	
	During the experiment you will be requested to follow a path (either a horizontal line or a triangle-shaped ramp) on a screen with a small cursor. The position of the cursor will change based on the amount of force you will exert. Since the relative effort is the same for every subject, we are able to compare different people, with different absolute levels of physical strength. 
	During one trial (usually the last), you may be requested to exert a certain (high) level of force and try to maintain it as long as you can (i.e. exhaustion).
	During the procedures you may feel tired and we will ask you to keep following the exercise to your best effort. You will obviously be free to stop at your convenience and decide to keep following the protocol after some resting or just interrupt the experiment at once. \\[.5cm]
			
	\subitem - Electrical stimulation
	
	In another case, you may be asked to maintain a certain level of force (low) for a longer period of time while we deliver mild electrical stimulations to your muscle. 
	This test should not be fatiguing nor cause discomfort. As a matter of fact, the contraction you will be asked to sustain is very moderate and the electrical stimulation barely perceivable. As a comparison: the current of electrical stimulation for muscle growth or training -which is considered to be non painful- is hundreds of times greater than the ones employed for our test.
	Once again, whether the stimulation would prove unpleasant, we will interrupt it immediately. Additionally, you will be equipped with a "panic button" that will stop the protocol instantly.\\[.5cm]    
	
	\subitem - Medical imaging  
	  
	In another occasion you may be asked to undergo a medical imaging measurement. Magnetic resonance- (MRI) or diffusion tensor (DTI) imaging are used to obtain three-dimensional representations of muscle's anatomy. The results can be used to create realistic muscle models hence to be utilized in complex simulations. This procedure is non-invasive and not painful but, due to the fact that the measurements involve laying down for a time varying between a few minutes and two hours, in a relatively small and noisy space, some people may feel discomfort. Once again, you will be equipped with a "panic button" that will interrupt the procedure instantly.\\[.5cm]
	 
	\item Risks and benefits
	
	This project is not directly beneficial to you (although you will know more about your blood pressure during the experiment), but could be extremely useful for patients who are wearing prostheses/orthoses and for a better understanding of the mechanisms of motor control. You are obviously welcome to get a copy of your specific results and discuss them with us, if you like. 
	
	We do not envisage adverse effects other than the ones we have already mentioned. In case non-research-related incidents occur, the experiment will be immediately interrupted and the collected data will be excluded. Further, we will make sure that we call, like in all other health-related incidents or emergencies, medical help. If you think that you have anyhow suffered a research-related injury, you should promptly notify the Principal Investigator listed in the Contact Information at the beginning of this form. Any cost occurred through medical treatment will need to be billed in the ordinary manner, to you or your insurance.\\[.5cm]
	
	\item Outcome measures
	
	We are planning on extracting information from your muscular activity and your force traces. From the sEMG signal we can decompose the recruitment strategies of individual motor neurons and understand what would change when blood flow or external pressure changes. At the same time we will record your blood pressure (systolic and diastolic). If medical imaging is performed, the imaging data will be used to derive computational models of the muscles.\\[.5cm]
	
	\item Privacy and data management
	
	We take privacy very seriously. Your name will always be kept separated from your data and stored securely in our offices. Your data will be stored in password-protected and encrypted servers and none except the researchers involved in this project will have access to the raw data. We may share the anonymized data with other research centres (and only with them), but we will never disclose any personal information of yours to anyone.\\[.5cm]
	
	\item exclusion criteria
	
	Whilst the experiment is safe and the risks of the involved procedures are extremely limited, some criteria should be fulfilled before you can be admitted to the study. As for this project, you should not participate if you:
		\subitem - are or you suspect you may be pregnant
		\subitem - you suffer for capillary and/or skin frailty, deep veins thrombosis, haemophilia or any disease correlated with altered (i.e., either reduced or increased) blood coagulation
		\subitem - you use antidepressant, blood thinning/thickening or pain drugs
		\subitem - you suffer for hypertension (high blood pressure)
		\subitem - you suffer (or have suffered) for a neurological disease 
		\subitem - or traumas in the last 6 months in the districts of interest (i.e. upper or lower limb)
		\subitem - you had neural or muscular pain which required medical attention in the last six months
		\subitem - you are following a strength-oriented training regimen
		\subitem - you feel uncomfortable in closed spaces and/or you carry unremovable metallic objects (e.g., hip prostheses, orthopaedic screws, pacemakers or similar) - this is a soft constrain, since it only applies for experiments involving medical imaging. You can just inform us that you do not feel like/you are not suited to undergo this procedure and you will be included only in the EMG$/$BFR experiments.\\[.5cm]
		
	\item Final note
	
	Please remember that you can withdraw from the experiment (or the entire project) at \textbf{any} time without explanation or prior notice.
	We will destroy all the data we have collected upon specific request.
	We are here to answer all your questions at the best of our knowledge, feel free to ask!
	
\end{itemize}

