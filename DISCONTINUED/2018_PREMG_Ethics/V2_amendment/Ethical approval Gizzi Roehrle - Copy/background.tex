	\subsection{Electromyography and external pressure}
	EMG recordings are used to investigate the activity of muscle fibres and to ultimately decode the control strategies of the central nervous system. This type of measurements is routinely used for basic physiology studies, diagnosis of neural/muscular diseases and to interpret subjects' intention to control prostheses and orthoses. 
	Whilst classical EMG relies on needles or thin wires inserted directly inside the muscles, contemporary technologies rely on surface electrodes which are applied quickly, non-invasively and with minimal discomfort. The main drawbacks of the this procedure are a reduced signal-to-noise ratio, the risk of movement artefacts and a more pronounced external noise contamination \cite{Merletti2004}.
	Modern technologies, such as high-density EMG (HDEMG), allow to harvest an unprecedented amount of information \cite{Merletti2003} from a spatially (and temporally) rich signal: by comparing the signal across several locations it is possible to use advanced blind source separation techniques (e.g., convolution kernel compensation) to identify the contribution of individual motor units  \cite{Holobar2007}. Motor units and spatial distribution of the muscular activity provide important insights in determining the overall muscle control in healthy individuals as well as, for example, individuals with pathological conditions such as chronic pain  \cite{Falla2014,Testa2017}.
	
	During field applications surface electrodes are often subject to strong external forces with consequences on the signal quality. Those forces, however, are unavoidable in real-life situations and a better understanding of how they influence the EMG signal may significantly increase the robustness of control systems and the reliability of parameter estimations. As an example one can consider the daily use of upper limb prostheses: external forces, e.g., such as those related to picking up an object, can increase the pressure of the socket against subject's skin and deform the soft tissues of the stump. The changes in the distance between the active muscle fibres and the electrodes  may result in changes of the amplitude and spectral characteristic of the EMG signal. This has unpredictable consequences on classification accuracy, which could ultimately affect the usability of the device.  
	
	
	\subsection{Blood flow restriction}
	External pressure causes a compression of the affected soft tissues, including blood vessels. This may result in restricting the blood flow (ischaemia) in the anatomical districts distal to the application point. 
	The lack of blood circulation has profound metabolic and neural consequences (briefly reported hereby), and represents an elegant model to investigate human neuromechanics.
	
	\subsubsection{Metabolic effect of BFR}
 	BFR causes a quick depletion of the locally available energy resources (such as phosphocreatine) and oxygen, together with an increase of catabolites and H\textsuperscript{+} ions \cite{Yanagisawa2017}. This causes an increase in muscle protein synthesis as early as 3 hours after a single bout of exercise \cite{Fujita2007,Gundermann2012,Fry2010}.
	
	Oxygen deprivation may also cause a selective decrease in the performance of type I muscle fibres (which rely on aerobic metabolism) forcing the type II fibres (anaerobic) to provide the necessary force. 
	This phenomenon, in contrast with the physiological order of recruitment \cite{HENNEMAN1965}, has been suggested as the cornerstone of training regimens that aim at maximizing force and muscle hypertrophy.
	
	High load exercise ($>$70\% of 1 repetition maximum, i.e., the highest weight that a subject can lift once, often reported as 1RM) is generally required to increase muscle size and strength over time \cite{Yanagisawa2017}. Recent studies, however, suggested that while temporary BFR is enforced, loads as low as 30\% of 1RM may be sufficient to elicit a comparable training effect \cite{Loenneke2012,Abe2012}, while reducing the signs of exercise-induced tissue damage \cite{Thiebaud2013}. 
	
	\subsubsection{BFR and motor control}
	Motor control adjustments can be assessed by looking into the muscular signal and the subjects' mechanical output. Since the neural inputs from supraspinal and spinal centres are common to the entire motor neuron pool, it is possible to investigate changes in the CNS control strategies by estimating the commonality across the motor neurons activations during a voluntary contraction. This implies computing the power of common oscillation frequencies, which allows to identify inputs from different volleys, normally represented in separated frequency bands \cite{Yavuz2015}. Another effect of transitory ischemia is the reduction, even close to elimination, of the 6-10Hz physiological tremor, which is associated to MU synchrony. This effect is accompanied by a drastic reduction of the amplitude of the corresponding force oscillation. Furthermore, removal of the occlusion reinstated the coherent MU rhythms and the initial oscillation (\cite{Erimaki2008}).
	
	In summary, blood flow restriction causes biophysical and metabolic alterations which result in  acute adjustments of the supraspinal and spinal control of muscle contraction. Neuromuscular activation seems to be increased during low level BFR exercise \cite{Yasuda2013,Yasuda2014} and this could be correlated to the anticipation of type II motor units recruitment. 
	
	Literature has identified two main mechanisms correlated with transitory BFR, namely: the early onset of peripheral fatigue and the presence of damped sensory information from the muscle and skin receptors.
	\begin{itemize}
			\item Peripheral fatigue  
	\end{itemize}
	
		The reduction in EMG mean or median frequency during occlusion is very much associated with the decrease in conduction velocity due to the blood flow restriction (\cite{Merletti1984}). The mechanisms that could contribute to the fatigue-induced decline in motor neuron firing rate include intrinsic motoneuronal properties (\cite{Kernell1982,Kernell1982a,Spielmann1993,Sawczuk1997}), changes in net input from other muscle receptors (e.g., muscle spindles \cite{Bongiovanni1990,Gandevia1990,Macefield1991,Macefield1993}, and a decline in effective descending drive to the motor neurons (\cite{Gandevia1996,Taylor2000}). 
		
		\begin{itemize}
		\item 	Afferent input dampening
		\end{itemize}
	  
		Sensory information flows from muscle spindles and cutaneous receptors to the spinal cord. Transitory BFR seem to alter its rate and content: the most popular explanations for this phenomenon are (i) that the altered activity of group III and IV muscle afferents reflexively inhibits spinal motor neurons (\cite{Garland2002}) and (ii) that independently on blood flow restriction, the local pressure alters the activity of primary spindle afferents due to the ischemic block itself (\cite{Grey2001}).
		
		The role of sensory feedback in this adjusted control pattern is also utmost important. Although the spectroscopic method reveals the contribution of sensory feedback, it cannot fully explain the mechanism of modulation. However, muscle reflex investigation is a utility method that may indicate the \textit{direction} (facilitated or inhibited activation) of modulation in sensory input \cite{Yavuz2015}. Testing voluntary contractions and reflexes, during BFR enables us to make an association between the modulation of sensory input excitability and variation in neuromuscular control strategy, due to the external pressure, see also Subsection \ref{stimulate}.

\begin{comment}
	Temporary BFR represents,therefore, an elegant experimental model for investigating different aspects of human neuromechanics: while it has no long-term consequences (provided an acute exposure), it can cause a series of adjustments that, taken together can significantly improve our understanding of motor control.
	Local blood flow restriction results in an immediate reduction of the local body temperature, which stiffens the muscle-tendon complex, influencing the mechanical response of the whole system (\cite{Ce2014,Yavuz2010,Zhou1998}). 
\end{comment}