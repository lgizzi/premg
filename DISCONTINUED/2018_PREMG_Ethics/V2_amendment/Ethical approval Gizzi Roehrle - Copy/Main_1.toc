\babel@toc {ngerman}{}
\babel@toc {british}{}
\contentsline {section}{\numberline {1}Short Summary of the Project}{4}%
\contentsline {section}{\numberline {2}Responsibilities and Address}{4}%
\contentsline {section}{\numberline {3}Scientific background}{5}%
\contentsline {subsection}{\numberline {3.1}Electromyography and external pressure}{5}%
\contentsline {subsection}{\numberline {3.2}Blood flow restriction}{5}%
\contentsline {subsubsection}{\numberline {3.2.1}Metabolic effect of BFR}{5}%
\contentsline {subsubsection}{\numberline {3.2.2}BFR and motor control}{5}%
\contentsline {subsection}{\numberline {3.3}Current project}{6}%
\contentsline {subsection}{\numberline {3.4}Experimental description}{6}%
\contentsline {subsubsection}{\numberline {3.4.1}Preparation}{6}%
\contentsline {subsubsection}{\numberline {3.4.2}Warm up and familiarization protocol}{7}%
\contentsline {subsubsection}{\numberline {3.4.3}MVC protocol}{7}%
\contentsline {subsubsection}{\numberline {3.4.4}Voluntary protocol}{7}%
\contentsline {subsubsection}{\numberline {3.4.5}Stimulation protocol}{7}%
\contentsline {subsubsection}{\numberline {3.4.6}Fatiguing protocol}{8}%
\contentsline {subsubsection}{\numberline {3.4.7}Medical imaging}{8}%
\contentsline {subsection}{\numberline {3.5}Experimental Protocol}{8}%
\contentsline {subsection}{\numberline {3.6}Risk/benefit analysis}{9}%
\contentsline {subsection}{\numberline {3.7}Summary}{9}%
\contentsline {section}{\numberline {4}Aim of the project}{10}%
\contentsline {subsection}{\numberline {4.1}Design of the project}{10}%
\contentsline {subsection}{\numberline {4.2}Outcome Measures}{10}%
\contentsline {section}{\numberline {5}Subjects and recruitment}{10}%
\contentsline {subsection}{\numberline {5.1}Number of subjects for the duration of the project}{10}%
\contentsline {subsection}{\numberline {5.2}Subjects selection}{10}%
\contentsline {subsubsection}{\numberline {5.2.1}Inclusion criteria}{10}%
\contentsline {subsubsection}{\numberline {5.2.2}Exclusion criteria}{10}%
\contentsline {subsubsection}{\numberline {5.2.3}Personal data}{10}%
\contentsline {subsubsection}{\numberline {5.2.4}Measurement parameters and laboratory parameters}{11}%
\contentsline {section}{\numberline {6}Trial Medication}{11}%
\contentsline {section}{\numberline {7}Treatment of the Subjects}{11}%
\contentsline {subsection}{\numberline {7.1}Admission to the study}{11}%
\contentsline {subsection}{\numberline {7.2}Hospitalisation}{11}%
\contentsline {subsection}{\numberline {7.3}Alimentation}{11}%
\contentsline {subsection}{\numberline {7.4}Concomitant Therapy}{11}%
\contentsline {subsection}{\numberline {7.5}Documentation}{11}%
\contentsline {section}{\numberline {8}Methods}{11}%
\contentsline {subsection}{\numberline {8.1}Electromyography (EMG), joint torque, blood pressure measurements and imaging}{11}%
\contentsline {subsection}{\numberline {8.2}Validation of the Measurement Methods}{12}%
\contentsline {subsection}{\numberline {8.3}Trustworthiness of the Data}{12}%
\contentsline {subsection}{\numberline {8.4}Recording of the Compliance}{12}%
\contentsline {subsection}{\numberline {8.5}Safety Committee}{12}%
\contentsline {subsection}{\numberline {8.6}Interruption of the study}{13}%
\contentsline {subsubsection}{\numberline {8.6.1}Interruption Criteria}{13}%
\contentsline {subsubsection}{\numberline {8.6.2}Interruption of One Subject (drop-out)}{13}%
\contentsline {subsubsection}{\numberline {8.6.3}Interruption of the Whole Project}{13}%
\contentsline {section}{\numberline {9}Changes in the Case Record Form}{13}%
\contentsline {section}{\numberline {10}Ethical and Legal Issues}{13}%
\contentsline {subsection}{\numberline {10.1}Legal Regulation}{13}%
\contentsline {subsection}{\numberline {10.2}Approval of the Ethics Committee}{13}%
\contentsline {subsection}{\numberline {10.3}Insurance}{13}%
\contentsline {subsection}{\numberline {10.4}Principal Investigator of the Research Project}{13}%
\contentsline {subsection}{\numberline {10.5}Investigator of the Research Project}{14}%
\contentsline {subsection}{\numberline {10.6}Multicentre Research}{14}%
\contentsline {subsection}{\numberline {10.7}Storage and Data Privacy Protection}{14}%
\contentsline {section}{\numberline {11}Publication}{14}%
\contentsline {section}{\numberline {12}Signatures}{17}%
\contentsline {section}{\numberline {13}Appendix: Informed Consent Form}{18}%
\babel@toc {ngerman}{}
\babel@toc {british}{}
\contentsline {section}{\numberline {14}Appendix: Data collection sheet and case report}{22}%
\contentsline {section}{\numberline {15}Appendix: Informative material for the volunteers}{26}%
\babel@toc {ngerman}{}
\babel@toc {british}{}
